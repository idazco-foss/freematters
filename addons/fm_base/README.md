FreeMatters Module

# Gap items

## Matter Notes

Matters needed Activity notes and File notes. Activity notes are for internal reference by the law firm
only and are used for making internal notes on the activity of a matter. File notes are the notes on a
matter that are for reporting and sharing with clients and courts as part of the file/matter.

These features will be in the `fm_base` dependent on the
[tagged_notes](https://github.com/idazco/odoo-addons/tree/13/tagged_notes) module.

#### Status:
The dependency is added. It provides the model only and this project will not implement a UI for Odoo.

---

## Project Key

The project key is the matter ID. To comply with LEDES it may be no longer than 20 characters. Allow
users to configure their own matter number format for auto generation. Implement a default format which
should be the number-dot-number format.

Handled in `fm_base` dependent on `project_key`.

---

## Project Contacts

Contact linking that the project module needs to be able to support:

 - Contact link tags for "partner" records tagged to the project. Basically this means that
the **link** from the contact to the project is tagged as opposed to tagging the contact record.
In other words, we are not setting a tagged context for the partner record on its own, instead
we are setting the tagged context for how the contact is linked to the project. This is because it
is possible for a contact to have different tags based on how they are linked to a project. For
example when a contact is linked as a client to a project then only in that context are they client.
Later that contact might be linked to a project as an contact in opposition in a case (Opposing Contact).

    1. Opposing Counsel  
    2. Opposing Contacts  
    3. Clients
    4. Client Management

**For this we are going to wait for the `project_contact` (OG) module.**

`project_contact` will support, in a generic way, linking tagged contacts to a project but will not define
the tags. `fm_base` will use `project_contact` for the linking but also provide the data which define the tags
for those links.

The standard contact record form view will need to change to include a "Projects" tab which will show a tree
view of all the projects that contact is linked to and how that link is tagged:

![project-contacts](readme_static/contacts-projects-links.png)

---

## Project Model Fields

Matter projects need to have some key fields of data which:

- define very high level matter information
- drive workflow and rules

These key fields are

1. matter type ("Transaction" / "Litigation") 
2. whether or not the matter claims "Assets" being involved and if so:
    
  - asset type ( real esate / debt / automotive / household / business / stock / ..other user defined)
  - asset legal description
  - asset docs (one to many link from asset to many existing docs)
  - asset location (an address)
  - asset value
  - asset valuation date
  - asset valuation method (user defined list)
  - asset in possession of (client/opposition)

--- 

## Court Cases

Support for court cases records are needed which will track:

  1. Link to the court "partner" record (company) record for that case
  2. Links to case docs (one to many from case to docs - one doc can link to multiple cases)
  3. Case ID  
  4. Case Note
  5. Case filing date
  6. Case filing fee (link to `project_ledger` record) 

---

## Billing Rates

Need support for billing rates. Perhaps it should be based on sales order? Billing rates should be linked
to project (matter), one at a time in chronological order. The latest confirmed sales order is the one that is
used to determine the charges for invoicing.

In the `sale_management` module it provides support only for the following 2
"Billing Type" options "At Project Rate" and "At Employee Rate". Neither of these are valid options for
law firms and we need to add a "By Rate Table" option. Invoicing functionality will have to be added
to support this new billing type. Currently the Sales Order cannot be changed for the project. For a law
firm, if the contract is renegotiated then one must be able to assign a new sales order to the project.

I think there should be a `project_contracts` module which focuses on managing the contracted rates for a
project and how and when they change. We also need to think about and document a workflow for changing
contract rates in relation to when invoicing occurs. The `project_contracts` module can actually be reused
for other things such as event management and construction.

---

## Billing

Timesheets and Materials need to ~~have~~ show additional columns for:

  - product id (to know what rate to charge based on billing rate or default rate)
    - this is already part of the model but its not shown
    - sales order id is not relevant and will not be used in FM
  - billable flag (to know whether the entry should be invoiced)
  - invoice link (to know if the timesheet entry was invoiced)
    - is this already part of the model?

The `project_ledger` (OG) must **NOT** handle these requirements, in order to maintain separation of concerns.

We're not sure yet if this should be in the `fm_base` module or a separate module with more generic application.
We can already see how this feature would be important to any business where the `project_contracts` module
would be used.

---

## Trust Accounting

The trust accounting will be handled by the `account_trust` (OG) module. This module needs to be able to work
for law firms and title companies. 
