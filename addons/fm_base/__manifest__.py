# -*- coding: utf-8 -*-
{
    'name': "FreeMatters Base Module",
    'summary': "A free and open source solution for law firms that need a basic system to run their practice.",
    'description': "The FreeMatters.org base module.",
    'author': "Mike Mugge",
    'website': "https://www.freematters.org",
    'version': '0.1',
    "application": True,
    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'project', 'hr', 'contacts', 'hr_timesheet',
                'project_category', 'project_status', 'project_key', 'project_task_add_very_high',
                'project_task_dependency', 'project_task_material', 'project_ledger', 'tagged_notes',
                ],
    # always loaded
    'data': [
        'data/notes_tags.xml',
        # 'views/views.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
