# -*- coding: utf-8 -*-
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).

from odoo import _, api, fields, models
import logging

_logger = logging.getLogger(__name__)


class ProjectTaggedNotes(models.Model):
    _inherit = 'notes.tagged'

    @api.model
    def add_project_note(self, values):
        _logger.info(values)
