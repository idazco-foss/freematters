#!/usr/bin/env bash

docker run --rm -it --name fm_traefik \
--network="host" \
--add-host="odoo13:127.0.0.1" \
-v $PWD/docker-traefik.yml:/etc/traefik/traefik.yml:ro \
traefik:v2.2