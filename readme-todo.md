## Major Todo Items:

 - Add proxy server to Docker stack (with Lets Encrypt support)
 - Code fm_base module with considerations for LEDES standards
 - Code fm_graphql module
 - Add replacement UI based on Quasar
 - Add basic reports
 - Add wizards