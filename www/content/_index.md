---
publishdate: 2020-06-14T00:00:00Z
date: 2020-06-14T00:00:00Z
title: Home
draft: false
menu: main
---

# FreeMatters

FreeMatters is a PROJECT for developing a free, open-source software package for managing a law firm.
It's based on the well known [Odoo ERP](https://www.youtube.com/watch?v=R_vEnMgUYQk)
platform and uses Odoo Community Edition as the foundation so that the software remains completely
free for use. Free Matters itself, should not be thought of as a stand-alone product. Its a project.

The software package, or "stack", can be used as system for managing a law firm or it can be used as an
intermediary data warehouse.

## Why not use Odoo as-is for managing matters?

Odoo effectively provides just a fundamental basis for managing a law firm, however, Odoo was originally
developed with manufacturing concerns as is core competency. To be clear, Odoo is not a product that
is developed primarily for managing a law firm. Therefore, there are many other aspects of managing a law
firm that are not part of the standard Odoo offering.

The second issue is that Odoo is an enterprise resource planning (ERP) system with a focus on accounting.
Attorneys and paralegals are not accountants and don't typically spend time in ERP platforms. Therefore
the user interface in Odoo is not optimal for use by law firms.

To overcome these two huge obstacles in Odoo implementation in law firms, we are continually developing
the Free Matters project to provide a proper package for 

---

## Quick Start Instructions

The fastest and simplest way to launch the project is with Docker. You will need to have Docker and
specifically `docker-compose` installed to perform the following steps. This process will use the
`docker-compose.yml` file to get the project running as Docker container. From here on, we will assume
that you know your way around using Docker.

A bash script is provided to get the stack running. If you are using Windows, then you should use
a Linux virtual machine to run the project. The project is based on Odoo and running Odoo on Windows
is not recommended from out point of view.

  1. Download the project or clone it using git.
  2. To start the Docker stack, simply run the `start-docker-stack.sh` script in the root directory.
  3. In your browser, navigate to http://localhost:8080 and login with `admin` for the user and password.  
