---
publishdate: 2020-06-13T00:00:00Z
date: 2020-06-14T00:00:00Z
title: Contacts
draft: false
menu: main
weight: 10
---

Contacts are the primary type of record to start working with. Contacts encompasses all data
that represents any people or organizations that you work with, have contact with for business
or need to reference in any other way, such as in matters, invoices, purchase orders or your
web portal. Contacts are referenced in any of these other modules, by linking them to records
in those modules. You can think of the Contacts module as your global address book.

There are 2 intrinsic types of contacts namely, "individual" and "organization". Any contact
can only be one of these types. Any "individual" contact record can have only one primary link to
any "organization" record and that link will define the primary organization context for that
individual. For example, one can have a record for an individual named "Frank Caprio" with the
title "Judge", linked to an organization record, where the organization is a court. Then it would
be clear that Frank Caprio, is a judge at that court.

It is possible though, that this individual, Frank Caprio, could be involved in a matter, outside
of their primary context, which is being a judge at a specific court. For example, that person
might be a client in a matter, where they have your firm draft a real estate purchase contract.
So then that individual could be linked to a specific matter with a defined context for linke to
the matter, by using a "tag". In this example, when Frank Caprio is linked to the real estate
purchase contract matter, one would tank that link, as "Client". This would make it clear that the
individuals relationship to the matter is as a client. Linking that individual contact record for
Frank Caprio, to that matter, as a "Client", will not change the fact that, in your global address
book of Contacts, that that individual is primarily linked to the court.

#### Important
It is not necessary to link an individual record to any organization record, however, when linking
any contact record to a matter, one must "tag" that link to define how that contact relates to the
matter.

