---
publishdate: 2020-06-14T00:00:00Z
date: 2020-06-14T00:00:00Z
title: Matters
draft: false
menu: main
weight: 20
---

A matter can be thought of as a project that needs to be managed. In fact, in Free Matters,
the Matters module is based on the Odoo projects module and a matter is basically an Odoo
project record with some special adaptations that make it appropriate for effectively tracking
legal matter data. This is the established paradigm for using Odoo for managing legal matters.

After establishing your database of contacts in the Contacts module, the Matters module becomes
the central module for doing your work. Of course however, Contact records can still be added
and updated at any time. One could even add a contact record directly from a matter record.
In Free Matters, we aim to make it possible to manage all relevant data from the Matters module
so that attorneys and paralegals dont need to constantly switch between modules in a basic workflow.

From a matter record, one can manage (among other things):

 - links to contacts
 - billing, invoices and payments
 - billing rate contracts
 - documents and emails
 - general and internal notes
 - activity logs
 - tasks, timesheets and appointments
 - settlement ledger and assets
 - court cases

So, with the exception of court cases, its easy to see how all these features apply to general
project management as well. The difference is that Free Matters adds all additional fields and
features needed for legal matter management and presents it in a simple to use interface.
   


