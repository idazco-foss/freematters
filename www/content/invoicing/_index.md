---
publishdate: 2020-06-14T00:00:00Z
date: 2020-06-14T00:00:00Z
title: Invoicing
draft: false
menu: main
weight: 90
---

Invoicing for any matter is based on timesheet entries and expenses incurred on a matter.
An invoice can be generated directly from a matter, or for all matters from the Invoice
module.

Matters can also track a "settlement ledger", however since records in that ledger are
not the firm's (time and expense) billing entries, they are never factored in when generating
an invoice. If an invoice or any part of an invoice, needs to be part of a settlement, it
can be added in whole or in part, to the settlement ledger.

Invoices always generate as "draft" invoices, however they can be bulk converted as "confirmed"
invoices in the Invoices module, or "confirmed" individually from a matter.

You can bulk email invoice notices from the Invoices module or individually from a matter.
The system will not email the invoices, but instead email a notification to the client to
login to their portal to download the invoice.

Invoice templates support limited customization where one can add a custom company logo and
a custom text footer.


