#!/usr/bin/env bash
cd "$(dirname "$0")"
rm -rf ../public/*
hugo -d ../public/
git add ../public/*