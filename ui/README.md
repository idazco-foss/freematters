# User Interface

This is essentially a sub project of the Free Matters project. It provides an uncluttered
user interface as a Single Page Application ([SPA](https://en.wikipedia.org/wiki/Single-page_application))

While in many cases a standard Odoo based UI could be sufficient for many users, its important
to recognize that Odoo is a complex ERP suite, and its complexity typically comes through in
its user interface. This usually means that there is a steep learning curve to using it, which
many law firms consider impractical.

For law firms that want to use their own technical staff to customize Free Matters, they will find
that this requires learning Odoo's QWeb framework which is extremely niche and also has a steep
learning curve. By comparison, Vuejs is a very ubiquitous framework, that is easy to learn and very
well documented. For this reason its easier to find developers with previous Vuejs experience.
Also its easier to attract developers for working with Vuejs, which is beneficial to their career
where ever they may go after your project is done, compared to hiring them for QWeb, which gives
them experience in something they are likely never to use again.

For the UI for Free Matters, we use the [Quasar framework](https://quasar.dev/introduction-to-quasar)
which is based on Vuejs. Besides making development of the SPA much easier, Quasar also makes it
simple to package the app as a Multi-Platform Electron Desktop Application or even to turn it into
an Android or iOS application. Quasar is MIT licensed and open-source and the UI solution based on
Quasar is also distributed under the [MIT license](./license.txt).

---

## Technical

 - We use Vuex to manage data state
 - We use JSON-RPC to communicate with the Odoo backend
 - Server interfacing is managed as a mixin
 - Design goals  
   - Manage as much as possible from within a matter (reduced clicks)
   - Be as intuitive as possible - minimal training should be necessary
 - SPA UI priority over Odoo parity (UI features in the SPA might not exist in the Odoo UI)  
   - Eliminated maintaining UI features in two places
   - Focuses UI development priority on the SPA and deliverable of MVP
   - Keep Odoo usage focused on model design and server side functionality

