# FreeMatters

FreeMatters is a free and open source software platform for managing a law firm.
It's based on the well known [Odoo ERP](https://www.youtube.com/watch?v=R_vEnMgUYQk)
platform and uses Odoo Community Edition as the foundation so that the software remains completely
free for use. The stack is comprised of these parts:
 
- Odoo CE
- Select Odoo modules
- Tailored user interface 
- Reporting
- Postgres 12 database

We aim to make the platform complete enough so that users will have all the basics needed to run
their law firm. Basic features road-map includes:

- Double entry accounting
- Contact management
- Case management
- Collections
- Scheduling
- HOA Collections
- Document management
- Time tracking
- Bank reconciliation
- Trust accounting
- Invoicing
- Email integration
- Client and registrant portal
- Video conference integration
- Automated Actions

This project is currently in its infancy and will progress a lot over the coming weeks.

---

## Goals

 1. Provide a free Odoo setup that law firms can use to run their practice.
 2. Implement LEDES standards where ever it makes sense to do so.
 3. Replace the Odoo interface with a tailored interface for a typical attorney.
 4. Provide documentation and videos for instructional purposes.
 5. Keep improving the project.

---

## Philosophy

We believe that software for effectively running a law firm can and should be free to use.
Some law firms can usually afford consultants or in-house software developers to make customizations.
We dont believe that law firms should have to pay enormous software license fees and not have much
left over in their budget for customizations they need. By keeping this project free, we help ensure
that law firms can save on licensing fees and have more cash in their budget for customizations.
By keeping this a community driven project, we help to ensure that improvements coming from the community
lower the necessity for substantial customizations in the first place.

---

### External Odoo Addons

Note: When using Odoo for managing matters, the standard paradigm is to use the Project module and have each project in Odoo represent a
legal "matter" (or case "file"). Keeping this in mind, we use the following Odoo Community Association add-ons to extend the core
functionality of the project module and thereby add features for matter management.

 - [project_key](https://github.com/OCA/project/tree/13.0/project_key): Unique project key used for unique matter ID management
 - [project_ledger](https://github.com/odoogap/project_ledger): Enables tracking of different ledgers for a matter
 - [project_category](https://github.com/OCA/project/tree/13.0/project_category): For categorization of matters
 - [project_status](https://github.com/OCA/project/tree/13.0/project_status): Provides status management on individual matters
 - [project_task_add_very_high](https://github.com/OCA/project/tree/13.0/project_task_add_very_high): Adds two new levels of priority for tasks on matters
 - [project_task_dependency](https://github.com/OCA/project/tree/13.0/project_task_dependency): Enables the user to define dependencies (other tasks) of a task
 - [project_task_material](https://github.com/OCA/project/tree/13.0/project_task_material): Enables tracking of expenses for a matter
 - [tagged_notes](https://github.com/idazco/odoo-addons/tree/13/tagged_notes): Tagged notes that can be attached to matters

---

### FreeMatters modules

 - fm_base: The base custom module for this project. It specifies all the extra Odoo addo-ons noted above as dependencies and adds additional features. 

---

## More reading:

 - [Docker Quick-start](./_docker/readme-docker.md)
 - [Todo Items](./readme-dev.md)
 - [Developer Tips](./readme-todo.md)
